var fs = require('fs');
var path = require('path');
var devconfig = JSON.parse(fs.readFileSync(path.resolve('.','../ph_config/config/dev.json')));

var app = function () {
    switch (process.env.NODE_ENV) {
    	case 'local':
    	    return localconfig;

        case 'dev':
            return devconfig;

        case 'production':
            return prodconfig;

        default:
            throw new Error("Config variables not set");
    }
};
module.exports = app();
